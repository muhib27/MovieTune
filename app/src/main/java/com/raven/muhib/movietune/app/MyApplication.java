package com.raven.muhib.movietune.app;


import android.app.Application;
import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MyApplication extends Application {

    private static MyApplication mInstance;
    Realm realm;
    RealmConfiguration config;


    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);

        config = new RealmConfiguration.Builder()
                //.name("user.realm")
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        realm = Realm.getDefaultInstance();
//        clearAll(realm);
        mInstance = this;

    }
    public void RemoveDb(){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }
    public static Context getContext() {
        return mInstance;
    }


}
