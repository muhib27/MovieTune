package com.raven.muhib.movietune.utils;

/**
 * Created by user on 8/28/2017.
 */

public class JsonUtils {

    public static final String KEY_ID = "id";
    public static final String KEY_VOTE_COUNT = "vote_count";
    public static final String KEY_VIDEO = "video";
    public static final String KEY_VOTE_AVERAGE = "vote_average";
    public static final String KEY_TITLE = "title";
    public static final String KEY_POSTERPATH = "poster_path";
    public static final String KEY_ORIGINAL_LAN = "original_language";
    public static final String KEY_ORIGINAL_TITLE = "original_title";
    public static final String KEY_BACKUP_PATH = "backdrop_path";
    public static final String KEY_ADULT = "adult";
    public static final String KEY_OVERVIEW = "overview";
    public static final String KEY_RELEASE_DATE = "release_date";
}
