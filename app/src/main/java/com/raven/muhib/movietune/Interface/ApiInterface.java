package com.raven.muhib.movietune.Interface;

import com.raven.muhib.movietune.model.NewReleaseResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {
//    @Headers({"clientAgent : ANDROID", "version : 1"})
//    @POST("api/user/register")
//    Call<ServerResponse> getUserValidity(@Body MyUser userLoginCredential);

    @GET("now_playing?api_key=c37d3b40004717511adb2c1fb b15eda4& page=1 ")
    Call<NewReleaseResponse> getNewRealese();

}

