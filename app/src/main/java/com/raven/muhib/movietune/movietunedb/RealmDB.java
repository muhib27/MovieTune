package com.raven.muhib.movietune.movietunedb;

import android.util.Log;

import com.raven.muhib.movietune.app.MyApplication;
import com.raven.muhib.movietune.model.NewRealese;
import com.raven.muhib.movietune.model.NewRealeseModel;
import com.raven.muhib.movietune.utils.JsonUtils;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;


public class RealmDB extends MyApplication {

    Realm realm;
    private static final String NEWRELEASE = "newrelease";
    private static final String TOPRATED = "toprated";
    private static final String UPCOMING = "upcoming";
    private List<NewRealeseModel> newRealeseModelList;

    @Override
    public void onCreate() {
        super.onCreate();
        realm = Realm.getDefaultInstance();
        //realm.where(NewRealese.class).findAll().clear();
    }

    public void insertDB(final List<NewRealeseModel> list) {
        for (int i = 0; i < list.size(); i++) {
            boolean flag = checkExistance(realm, list.get(i).getId());
            if (!flag) {

                try {
                    NewRealeseModel newRealeseModel = new NewRealeseModel();
                    newRealeseModel.setId(1);
                    realm.copyToRealm(newRealeseModel);
                } catch (Exception e) {
                    Log.v("TAG", "Data insertion failed");
                }
            }
        }
    }


    public boolean checkExistance(Realm realm, int id) {
        RealmResults<NewRealeseModel> results = realm.where(NewRealeseModel.class)
                .equalTo(JsonUtils.KEY_ID, id)
                .findAll();
        //RealmResults<WaitingModel> waitingModelresult = realm.where(WaitingModel.class).equalTo(JsonUtils.KEY_ID, id).findAll();
        if (results.size() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public List<NewRealeseModel> getNewRealeseModelList = new ArrayList<>();

    public List<NewRealeseModel> getAllNewRelease(final Realm realm) {
        getNewRealeseModelList = new ArrayList<>();
        RealmResults<NewRealeseModel> results;
        try {

            results = realm.where(NewRealeseModel.class).findAll();
            for (int i = 0; i < results.size(); i++)
                getNewRealeseModelList.add(results.get(i));
        } catch (Exception e) {

        }
        return getNewRealeseModelList;
    }


}
