package com.raven.muhib.movietune.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.raven.muhib.movietune.Interface.ApiInterface;
import com.raven.muhib.movietune.R;
import com.raven.muhib.movietune.Retrofit.RestClient;
import com.raven.muhib.movietune.adapter.NewRealeaseAdapter;
import com.raven.muhib.movietune.model.NewRealese;
import com.raven.muhib.movietune.model.NewRealeseModel;
import com.raven.muhib.movietune.model.NewReleaseResponse;
import com.raven.muhib.movietune.movietunedb.RealmDB;
import com.raven.muhib.movietune.utils.NetworkConnection;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewRealeseFragment extends Fragment {
    NewRealeaseAdapter newRealeaseAdapter;
    private int statusCode;
    private String statusMessage = "";
    private ApiInterface apiInterface;
    private List<NewRealeseModel> newRealeseModelList;
    private List<NewRealeseModel> displyList;
    private JsonArray newReleaseJsonArray;

    private GridView gridView;
    RealmDB realmDB;
    private TextView emptyText;
    Realm realm;

    public NewRealeseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_realese, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        gridView = (GridView) view.findViewById(R.id.gridview);
        emptyText = (TextView) view.findViewById(R.id.empty);
        emptyText.setVisibility(View.GONE);
        realmDB = new RealmDB();
        realm = Realm.getDefaultInstance();
        newRealeseModelList = new ArrayList<>();
        displyList = new ArrayList<>();
        newReleaseJsonArray = new JsonArray();
//        progressDialog = ProgressDialog.show(getActivity(), "Connecting",
//                "Please wait...", true);
        displyList = realmDB.getAllNewRelease(realm);
        if (newRealeseModelList.size() <= 0)
            loadServerData();
        else
            loadData();
        newRealeaseAdapter = new NewRealeaseAdapter(getActivity(), displyList);
        gridView.setAdapter(newRealeaseAdapter);

    }

    ProgressDialog progressDialog;

    public void loadData() {
        if(newRealeaseAdapter==null) {
            newRealeaseAdapter = new NewRealeaseAdapter(getActivity(), displyList);
            gridView.setAdapter(newRealeaseAdapter);
        }
        else{
            newRealeaseAdapter.setData(displyList);
            newRealeaseAdapter.notifyDataSetChanged();
            gridView.setAdapter(newRealeaseAdapter);
        }
    }


    public void loadServerData() {
        if (!NetworkConnection.getInstance().isNetworkAvailable()) {
            Toast.makeText(getActivity(), "No Connectivity", Toast.LENGTH_SHORT).show();
            return;
        }
        if (progressDialog != null)
            progressDialog = ProgressDialog.show(getActivity(), "Connecting",
                    "Please wait...", true);
        apiInterface = RestClient.createService(ApiInterface.class);
        Call<NewReleaseResponse> call = apiInterface.getNewRealese();

        call.enqueue(new Callback<NewReleaseResponse>() {

            @Override
            public void onResponse(Call<NewReleaseResponse> call, Response<NewReleaseResponse> response) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                NewReleaseResponse newReleaseResponse = response.body();
                if (newReleaseResponse != null) {
//                    statusCode = newReleaseResponse.getStatusCode();
//                    statusMessage = newReleaseResponse.getStatusMessage();
//                    if (statusCode == 200) {
                        newRealeseModelList = newReleaseResponse.getNewRealesesList();

                        if(newRealeseModelList.size()>0) {
                            realm.beginTransaction();
                            realmDB.insertDB(newRealeseModelList);
                            realm.commitTransaction();
                        }
                    //}

                    Toast.makeText(getActivity(), "inserted", Toast.LENGTH_LONG).show();
                } else {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    Toast.makeText(getActivity(), "No data found", Toast.LENGTH_LONG).show();
                }
                loadData();
            }

            @Override
            public void onFailure(Call<NewReleaseResponse> call, Throwable t) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                Toast.makeText(getActivity(), "onFailure", Toast.LENGTH_LONG).show();
            }
        });
    }
}
