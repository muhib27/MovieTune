package com.raven.muhib.movietune.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.raven.muhib.movietune.R;
import com.raven.muhib.movietune.model.NewRealeseModel;

import java.util.List;

/**
 * Created by user on 8/24/2017.
 */

public class NewRealeaseAdapter extends BaseAdapter {

    private Context context;
    List<NewRealeseModel>newRealeseModelList;
    public NewRealeaseAdapter(Context context, List<NewRealeseModel>newRealeseModelList) {
        this.context = context;
        this.newRealeseModelList = newRealeseModelList;
    }

    @Override
    public int getCount() {
        return newRealeseModelList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    public void setData(List<NewRealeseModel> newRealeseModelList) {
        this.newRealeseModelList = newRealeseModelList;
    }

    @Override
    public View getView(int i, View rowView, ViewGroup viewGroup) {
        // TODO Auto-generated method stub
        View convertView = rowView;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
//            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.grid_item_layout, null);

            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
           // viewHolder.text = (TextView) rowView.findViewById(R.id.TextView01);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.movieCover);
            convertView.setTag(viewHolder);
            viewHolder.image.setImageResource(R.drawable.splash_screen);

            //grid = new View(context);
            //grid = inflater.inflate(R.layout.grid_item_layout, null);
//            TextView textView = (TextView) grid.findViewById(R.id.grid_text);
            //ImageView imageView = (ImageView)grid.findViewById(R.id.movieCover);
//            textView.setText(web[position]);
            //imageView.setImageResource(Imageid[i]);
        } else {
            convertView = (View) rowView;
        }

        return convertView;
    }
    static class ViewHolder {
        public TextView text;
        public ImageView image;
    }

}
