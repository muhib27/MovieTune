package com.raven.muhib.movietune.Retrofit;

import okhttp3.OkHttpClient;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RestClient {

    public static <S> S createService(Class<S> serviceClass) {

        String BASE_URL = "https://api.themoviedb.org/3/movie/";
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        OkHttpClient client = httpClient.build();
        Retrofit retrofit = null;

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(serviceClass);
    }

//    public static <S> S createServiceWithAuth(final MyHeader header, Class<S> serviceClass) {
//        String BASE_URL = "";
//
//        Retrofit retrofit = null;
//
//        Interceptor interceptor = new Interceptor() {
//            //MyHeader myHeader = new MyHeader();
//            String securityToken = header.getSecurityToken();
//            final String finalsecurityToken = "Bearer "+securityToken;
//            @Override
//            public Response intercept(Chain chain) throws IOException {
//                final Request request = chain.request().newBuilder()
//                        .addHeader("Authorization", finalsecurityToken)
//                        .build();
//
//                return chain.proceed(request);
//            }
//        };
//        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.addInterceptor(interceptor);
//        OkHttpClient client = httpClient.build();
//
//
//        retrofit = new Retrofit.Builder()
//                .baseUrl(BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .client(client)
//                .build();
//        return retrofit.create(serviceClass);
//    }
}
