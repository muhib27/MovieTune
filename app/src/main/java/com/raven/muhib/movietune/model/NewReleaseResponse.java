package com.raven.muhib.movietune.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmObject;

public class NewReleaseResponse extends RealmObject{

    @SerializedName("results")
    public List<NewRealeseModel> newRealesesList;
    @SerializedName("statusCode")
    private int statusCode;
    @SerializedName("statusMessage")
    private String statusMessage;

    public List<NewRealeseModel> getNewRealesesList() {
        return newRealesesList;
    }

    public void setNewRealesesList(List<NewRealeseModel> newRealesesList) {
        this.newRealesesList = newRealesesList;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
